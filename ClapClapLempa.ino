/*
Clap clap ON/OFF

The program looks for pulses from microphone module and sets output to HIGH if two 
claps in a series were detected. To avoid false turn ons/offs from random pulses 
or continious clapping, when the one pulse is detected, program for 500ms looks 
for another pulse. If there another pulse is detected in that time program stops for 500ms and
goes to the beginning.   

CIRCUIT:
*Arduino NANO (or other)
*Microphone with digital output of comparator built in it.

Program code is in public domain

Author Narunas Kapocius 
Version 1.0
2016 11 01

 */
// Initialization of pins



int micIn=4; // Digital pin 4 form microphone input
int relayOut=13; // Output pin to relay
int micDetect1, micDetect2, falseDetect, lightState, clap; 
unsigned long time1,time2,time3, difference1, difference2; // for time readings

void setup() {
  Serial.begin(115200); 
  // put your setup code here, to run once:
  pinMode(micIn, INPUT);
  pinMode(relayOut, OUTPUT);
// HERE I reset all values that program run correctly from the beggining
  difference1=0;
  difference2=0;
  time1=0;
  time2=0;
  time3=0;
  lightState=0;
}

void loop() {
micDetect1=digitalRead(micIn);
time1=millis();  
if(micDetect1==HIGH){
  delay(50);
  ///////////////////FIRST WHILE LOOP FOR DETECTING FALSE TRIGGERS 
  while(difference2<500){
    time3=millis();
    difference2=time3-time1;
    falseDetect=digitalRead(micIn);
    if(falseDetect==HIGH){
     delay(500);
     clap=0;
     difference1=4000;// random value bigger than difference1 and difference 2
     difference2=4000;
      }
    }
    ////////////////////SECOND WHILE LOOP FOR DETECTING SECOND CLAP IF NO FALSE TRIGGER WAS DETECTED 
  while(difference1<2500){
    time2=millis();
    micDetect2=digitalRead(micIn);
    difference1=time2-time1;
    Serial.println(difference1);
    if(micDetect2==HIGH){
     clap=2;
     difference1=4000;
    }
  }
 }

  ////////////////////// SET OUTPUT SIGNAL HIGH/LOW DEPENT ON CURRENT STATE 
   
if (clap==2&&lightState==0){
  delay(250);
  digitalWrite(relayOut, HIGH);
  lightState=1;
  clap=0;
  delay(250);
  }
else if (clap==2 && lightState==1){
  delay(250);
  digitalWrite(relayOut,LOW);
  lightState=0;
  clap=0;
  delay(250);
  }
  /////////////RESET WHILE LOOPS VARIABLES
 difference1=0;
 difference2=0;


}

  

  
  



